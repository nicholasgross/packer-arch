all:
	git clone https://github.com/Dearon/dotfiles.git
	packer build -only=virtualbox-iso arch-template.json
	vagrant box add arch packer_arch_virtualbox.box
	vagrant up --provision
	vagrant ssh
