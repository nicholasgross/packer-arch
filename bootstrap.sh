#!/usr/bin/env bash

pacman -Syu
pacman -S --noconfirm fabric
sudo su vagrant
pushd /home/vagrant
fab --fabfile=/vagrant/fabfile.py pacman
fab --fabfile=/vagrant/fabfile.py oh_my_zsh
fab --fabfile=/vagrant/fabfile.py install_vim
# fab --fabfile=/vagrant/fabfile.py update_vim

exit 0
