"""Docstring"""

from fabric.api import local

VIM_BUNDLES = [
    {
        'git': 'git://github.com/altercation/vim-colors-solarized.git',
        'path': '/home/vagrant/.vim/bundle/vim-colors-solarized'
    },
    {
        'git': 'git://git.wincent.com/command-t.git',
        'path': '/home/vagrant/.vim/bundle/command-t'
    },
    {
        'git': 'git://github.com/mileszs/ack.vim.git',
        'path': '/home/vagrant/.vim/bundle/ack.vim'
    },
    {
        'git': 'git://github.com/tpope/vim-fugitive.git',
        'path': '/home/vagrant/.vim/bundle/vim-fugitive'
    },
    {
        'git': 'git://github.com/sjl/gundo.vim.git',
        'path': '/home/vagrant/.vim/bundle/gundo.vim'
    },
    {
        'git': 'git://github.com/ervandew/supertab.git',
        'path': '/home/vagrant/.vim/bundle/supertab'
    },
    {
        'git': 'git://github.com/Raimondi/delimitMate.git',
        'path': '/home/vagrant/.vim/bundle/delimitMate'
    },
    {
        'git': 'git://github.com/docunext/closetag.vim.git',
        'path': '/home/vagrant/.vim/bundle/closetag.vim'
    },
    {
        'git': 'git://github.com/shawncplus/phpcomplete.vim.git',
        'path': '/home/vagrant/.vim/bundle/phpcomplete.vim'
    },
    {
        'git': 'git://github.com/majutsushi/tagbar.git',
        'path': '/home/vagrant/.vim/bundle/tagbar'
    },
    {
        'git': 'git://github.com/StanAngeloff/php.vim.git',
        'path': '/home/vagrant/.vim/bundle/php.vim'
    }
]

def pacman():
    """Docstring"""
    local('pacman --noconfirm -Syu')
    local('pacman -S --noconfirm git zsh vim lynx tk ack wget cabal-install \
           python2-psutil python2-pygit2 p7zip unrar unzip zip htop')

def oh_my_zsh():
    """Docstring"""
    local('runuser -l vagrant -c "curl -L http://install.ohmyz.sh | sh"')
    local('runuser -l vagrant -c "cp ~/.zshrc.pre-oh-my-zsh ~/.zshrc"')
    local('chsh -s /bin/zsh vagrant')
    local('cp /vagrant/dotfiles/.zshrc /home/vagrant/.zshrc') 
    local('chown vagrant:users /home/vagrant/.zshrc')

def install_vim():
    """Docstring"""
    local('mkdir -p /home/vagrant/.vim/autoload /home/vagrant/.vim/bundle')
    local('curl -LSso /home/vagrant/.vim/autoload/pathogen.vim \
            https://tpo.pe/pathogen.vim')

    for bundle in VIM_BUNDLES:
        local('git clone ' + bundle['git'] + ' ' + bundle['path'])

    local('cd /home/vagrant/.vim/bundle/command-t/ruby/command-t && \
            ruby extconf.rb && make')
    local('cd /home/vagrant')

def update_vim():
    """Docstring"""
    for bundle in VIM_BUNDLES:
        local('cd ' + bundle['path'] + ' && git pull')

    local('cd /home/vagrant')

